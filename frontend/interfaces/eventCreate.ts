// These are interfaces that belong to the /events/create directory

export interface Ticket {
    type: "PAID" | "FREE"
    name: string,
    quantity: string,
    price: string,
    startSelling: string,
    endSelling: string,
    saleStartTime: string,
    saleEndTime: string
}

export interface EventData{
    description: {
        title: string,
        organizer: string,
        category: string
    },
    date: {
        startDate: string,
        startTime: string,
        endDate: string,
        endTime: string,
        timeZone: string
    },
    details: {
        banner_url: string,
        thumb_url: string,
        description: string,
    },
    bookings: {
        tickets: Ticket[]
    },
    frequency: "SINGLE" | "RECURRING"
}
export interface Location{
    type: "VIRTUAL" | "PHYSICAL",
        physical: {
            address: string
        },
        virtual: {
            medium: string,
            link: string
        }
}
