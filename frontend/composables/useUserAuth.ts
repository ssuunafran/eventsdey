import { useEventDeyUser } from "./useEventDeyUser"

export const signInUser = async (email: string, password: string) => {

    const isSignedIn = await fetch("https://eventsdey-staging.onrender.com/api/v1/auth/sign-in", {
        method: "POST",
        body: JSON.stringify({
            user: {
                email: email,
                password: password
            }
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(async (res) => {
        if(res.ok){
            return res.json()
        }
        throw await res.json()
    })
    .then((data) => {
        console.log(data)
        initUser(data)
        return {success: true, message: 'Sucessfully signed in'}
    })
    .catch((error) => {
        console.log(error)
        console.log(error.message, error.error)
        if(error.message == 'Failed to fetch'){
            return {success: false, message: "No Internet Connection!"}
        }
        return {success: false, message: getErrorMessage(error)}
        



    })

    return isSignedIn
}

export const getErrorMessage = (error: any) => {
  return error?.message
    ? error?.message
    : error?.error === "Network Error"
    ? "Please check your network"
    : error?.error;
};


// export default function handleErrorMessage(error, options) {
//   const message = getErrorMessage(error);
//   if (typeof message === "string") {
//     toast(message, { type: "error", ...options });
//   } else if (typeof message === "object") {
//     message?.forEach(text => {
//       toast(text, { type: "error", autoClose: false, ...options });
//     });
//   }
// }

export const registerUser = async (fname: string, lname: string, email: string, password: string) => {
    
        const user_data = await fetch("https://eventsdey-staging.onrender.com/api/v1/auth/sign-up", {
            method: "POST",
            body: JSON.stringify({
                user: {
                    firstName: fname,
                    lastName: lname,
                    email: email,
                    password: password
                }
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((res) => {
            if (res.ok){
                return res.json()
            }
            throw res
        }).then((data) => {
            console.log(data)
            initUser(data)
            return true
        })
        .catch((error) => {
            console.log(error)
            return false
        })

        return user_data
    
    }

export const initUser = (user: any) => {
    const userCookie = useCookie('userCookie')

    const eventDeyUser = useEventDeyUser()

    userCookie.value = user
    eventDeyUser.value = user
    console.log('done')

}

export const signOutUser = () => {
    const userCookie = useCookie('userCookie')

    const eventDeyUser = useEventDeyUser()

    userCookie.value = null
    eventDeyUser.value = null
    console.log('signed out')

    console.log(userCookie.value) 
}