import {decodeJwtToken} from './decode';
import { useCounterStore } from '../../assets/store/store'

export function useUserData() {
  const userData = ref<any>(null); // Adjust the type according to your data structure
  const store = useCounterStore()

  function setUserDataFromToken() {
    const token =store.getToken

    console.log(`the token is ${JSON.stringify(token)}`) 

    if (token) {
      try {
        const decodedData = decodeJwtToken(token);
        userData.value = decodedData;
        console.log(`decode is ${JSON.stringify(userData.value)}`);
      } catch (error) {
        console.error('Error decoding JWT token:', error);
        // Handle the error as needed
      }
    }
  }

  // Call the function to set userdata.value from the token
  setUserDataFromToken();

  // Return the userdata ref and any other functions or data needed by components
  return {
    userData
  };
}
