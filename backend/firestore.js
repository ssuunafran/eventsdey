import { initializeApp, getAnalytics } from "firebase/app";
import { getFirestore, Timestamp, FieldValue  } from "firebase/firestore";


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDOMv7tIZMbJrxUvjgXXg5ujSJBPSgFd1w",
  authDomain: "eventsdey-23488.firebaseapp.com",
  databaseURL: "https://eventsdey-Timestamp, FieldValue 23488-default-rtdb.firebaseio.com",
  projectId: "eventsdey-23488",
  storageBucket: "eventsdey-23488.appspot.com",
  messagingSenderId: "949734304777",
  appId: "1:949734304777:web:f58b73d965aafb89659cd8",
  measurementId: "G-D76JENBF3B"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

// Now you can use Firestore
const db = getFirestore(app);

// You can use Timestamp, FieldValue, and other Firebase features as needed
const timestamp = Timestamp.fromDate(new Date());
const fieldValue = FieldValue.serverTimestamp();

// If you are using TypeScript, you can also import types
// import { Firestore, Timestamp, FieldValue } from 'firebase/firestore';
