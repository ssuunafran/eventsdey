import Swal from 'sweetalert2';
import { ref, onMounted } from 'vue';
import { useRouter } from 'vue-router';

export function useSuccessToast(pop:any,message:string) {
  const router = useRouter();

    if (pop.value) {
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.onmouseenter = Swal.stopTimer;
          toast.onmouseleave = Swal.resumeTimer;
        }
      });

      Toast.fire(message, '', 'success');

      const updatedQuery = { ...router.currentRoute.value.query };
      delete updatedQuery.pop;
      router.push({ query: updatedQuery });
      pop.value = false;
    }

}
