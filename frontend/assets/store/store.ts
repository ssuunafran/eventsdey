import { defineStore } from 'pinia';

export const useCounterStore = defineStore('AccessToken', {
  state: () => {
    return { accessToken:null as string | null }
  },
  getters: {
    // automatically infers the return type as a number
    getToken(state) {
      return state.accessToken
    }
  },
  // could also be defined as
  // state: () => ({ count: 0 })
  actions: {
    setToken(tokenPassed: string) {
      this.accessToken=tokenPassed
    },
    delToken(){
      this.accessToken=null
    }
  },
  persist: true,
})
