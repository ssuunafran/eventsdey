const firebase = require('./google-Auth/firebase')
const db = firebase.firestore()

    const writeToFirestore = (docRef, dataObj, res) => {
        docRef
          .set(dataObj)
          .then(() => {
            console.log("ok");
            res.status(200).json({ message: 'Data successfully sent to Firestore' });
          })
          .catch((error) => {
            console.error("Error writing document:", error);
            res.status(500).json({ message: "Internal Server Error" });
          });
      };

      module.exports={
        db,
        writeToFirestore,
      }