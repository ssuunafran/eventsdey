import { computed } from 'vue';

export const useFormattedDate = (originalDate) => {
  const formattedDate = computed(() => {
    const dateObject = new Date(originalDate.value);
    return dateObject.toLocaleDateString('en-US', { year: 'numeric', month: 'long', day: 'numeric' });
  });

  return formattedDate;
};

export const convertTo12HourFormat=(time24)=> {
  const [hours, minutes] = time24.split(':');
  const time = new Date(0, 0, 0, hours, minutes);

  const options = { hour: 'numeric', minute: 'numeric', hour12: true };
  return new Intl.DateTimeFormat('en-US', options).format(time);
}

