const jwt = require('jsonwebtoken');
const tokenDb = require('./redis')
const ACCESS_TOKEN_SECRET = '99999i9jijuiojijijijijijijijidjddddddddddddddddddiiiiiiiiiiiiiiiii'
const REFRESH_TOKEN_SECRET = '99999i9jijuioiiiiiiijijijijidjddddddddddddddddddiiiiiiiiiiiiiiiii'

function generateAccessToken(user) {
    return jwt.sign(user,ACCESS_TOKEN_SECRET , { expiresIn: '180s' })
}

function generateAndSetTokens(user, expiresIn, res) {
    // const user = { id: req.user };

    // Generate tokens
    const accessToken = generateAccessToken(user);
    const refreshToken = jwt.sign(user, REFRESH_TOKEN_SECRET);

    // Set refresh token in Redis
    tokenDb.set('refreshTokenkey', refreshToken, 'EX', expiresIn, (err, reply) => {
        if (err) {
            console.error(err);
            res.status(500).json({ message: 'Internal Server Error'});
        } else {
            res.status(200).json({
                accessToken: accessToken,
                message: 'Successfully Signed In',
                sessionMessage:'Session started'
            });
        }
    });
}

function verifyRefreshToken() {
    return (req, res, next) => {
        const incomingRefreshToken = req.headers['x-refresh-token'];

        if (incomingRefreshToken == null) {
            return res.sendStatus(401);
        }

        // Verify the refreshToken against the stored refresh tokens in Redis
        tokenDb.get('refreshTokenkey', (err, storedRefreshToken) => {
            if (err || storedRefreshToken === null || storedRefreshToken !== incomingRefreshToken) {
                return res.sendStatus(403);
            }

            // If the refresh token is found in Redis, verify it and generate a new access token
            jwt.verify(incomingRefreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
                if (err) {
                    return res.sendStatus(403);
                }

                // Attach the user object to the request for further use
                req.user = user;

                // Continue to the next middleware or route handler
                next();
            });
        });
    };
}

module.exports = {
    generateAndSetTokens,
    verifyRefreshToken
}