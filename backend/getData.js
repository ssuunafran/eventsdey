const fetchAllDocuments = async (collectionRef) => {
  try {
    const querySnapshot = await collectionRef.get();
    const documents = [];

    querySnapshot.forEach((doc) => {
      if (doc.exists) {
        documents.push(doc.data());
      }
    });

    return documents;
  } catch (error) {
    console.error(`Error fetching documents:`, error);
    throw error;
  }
};

const fetchDocumentById = async (collectionRef, documentId) => {
  try {
    // Create a DocumentReference directly using the provided documentId
    const docRef = collectionRef.doc(documentId);

    // Get the DocumentSnapshot
    const docSnap = await docRef.get();

    if (docSnap.exists) {
      // Document exists, return its data
      return docSnap.data();
    } else {
      // Document does not exist, return null or handle it as needed
      return null;
      // You could also throw an error here if a missing document is unexpected
    }
  } catch (error) {
    console.error(`Error fetching document:`, error);
    throw error; // Re-throw the error for proper handling upstream
  }
};

module.exports = {
  fetchDocumentById,
  fetchAllDocuments,
};