import jwt_decode from "jwt-decode";

export function decodeJwtToken(token:string) {
  try {
    const decoded = jwt_decode(token);
    return decoded;
  } catch (error) {
    console.error("Error decoding JWT token:", error);
    return null; // Or you can throw an error or handle it in another way
  }
}