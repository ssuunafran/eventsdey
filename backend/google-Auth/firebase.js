const firebase = require("firebase-admin");

const credentials = require("../key.json");

firebase.initializeApp({
  credential: firebase.credential.cert(credentials),
  databaseURL: "https://eventsdey-23488-default-rtdb.firebaseio.com",
});

module.exports = firebase;