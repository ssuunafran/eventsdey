import { initializeApp } from 'firebase/app';
import { getAuth, signInWithPopup,signOut } from 'firebase/auth';
import { GoogleAuthProvider } from 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyDOMv7tIZMbJrxUvjgXXg5ujSJBPSgFd1w",
  authDomain: "eventsdey-23488.firebaseapp.com",
  databaseURL: "https://eventsdey-23488-default-rtdb.firebaseio.com",
  projectId: "eventsdey-23488",
  storageBucket: "eventsdey-23488.appspot.com",
  messagingSenderId: "949734304777",
  appId: "1:949734304777:web:f58b73d965aafb89659cd8",
  measurementId: "G-D76JENBF3B"
};

const app = initializeApp(firebaseConfig);

const provider = new GoogleAuthProvider();
const auth = getAuth(app); // Pass the app instance to getAuth
provider.setCustomParameters({ prompt: 'select_account' });


export function SignOut(){
  signOut(auth).then(() => {
    // Sign-out successful.
    console.log('Signed out user')
  }).catch((error) => {
    // An error happened.
  });
}

export {auth,provider};

