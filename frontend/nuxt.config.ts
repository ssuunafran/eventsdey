// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    modules: [
      '@nuxt/image-edge',
      'nuxt-icon',
      'nuxt-primevue',
      '@pinia-plugin-persistedstate/nuxt',
      [
      "@pinia/nuxt",
      {
        autoImports: [
          // automatically imports `defineStore`
          "defineStore", // import { defineStore } from 'pinia'
        ],
      },
    ],
  ],
    app: {
      head: {
        title: 'Eventsdey',
        meta: [
          {name: 'description', content: 'Eventsdey events manager by Eventsbrite'}
        ]
      }
    },
    image: {
      cloudinary: {
        baseURL: 'https://res.cloudinary.com/dn6stpnn2/image/upload/'
      }
    },
    css: ['vuetify/lib/styles/main.sass', '@mdi/font/css/materialdesignicons.min.css', '@/styles/styles.scss'],
    build: {
      transpile: ['vuetify'],
    },
    vite: {
      css: {
        preprocessorOptions: {
          scss: {
            additionalData: `@import "@/styles/all.scss";`,
          },
        },
      },
    },
  
    router: {
      options: {
        linkActiveClass: 'active'
      }
    }
})
