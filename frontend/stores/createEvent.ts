import {defineStore} from 'pinia'
import type { EventData, Ticket, Location } from '~~/interfaces/eventCreate';
export const useCreateEvent = defineStore('createEvent', {
    state: () : {eventData: EventData, eventLocation: Location, images: {thumbnail: File | null, banner: File | null}} => ({
        eventData: {
            description: {
                title: '',
                organizer: "",
                category: ""
            },
            date: {
                startDate: '',
                startTime: '',
                endDate: '',
                endTime: '',
                timeZone: ''
            },
            details: {
                banner_url: '',
                thumb_url: '',
                description: '',
            },
            bookings: {
                tickets: []
            },
            frequency: "SINGLE"
        },
        eventLocation: {
            type: "PHYSICAL",
            physical: {
                address: ''
            },
            virtual: {
                medium: '',
                link: ''
            }
        },
        images: {
            thumbnail: null,
            banner: null
        }
    }),
    actions: {
        // setLocation ({country, state}: {country: string, state: string}) {
        //     console.log(country, state);
        //     this.eventLocation.physical.country = country;
        //     this.eventLocation.physical.state = state;
        // },
        setDescription(content: string){
            console.log(content)
            this.eventData.details.description = content
        },
        setBanner(url: Blob, file: File){
            this.images.banner = file
            this.eventData.details.banner_url = url.toString()
        },
        setThumb(url: Blob, file: File){
            console.log(`the image url is ${url} and file is ${file} `)
            this.eventData.details.thumb_url = url.toString()
            this.images.thumbnail = file
        },
        createSection(data: Ticket){
            this.eventData.bookings.tickets.push(data)
        }

    }
})