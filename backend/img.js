const cloudinary = require('cloudinary').v2;

cloudinary.config({
    cloud_name: 'dn6stpnn2',
    api_key: '955786558773522',
    api_secret: 'g1flttorNht3OSkJEWxJLay6wE8'
});

async function handleUpload(file, imag, typ) {
    return new Promise((resolve, reject) => {
      cloudinary.uploader.upload(
        file,
        {
          folder: "eventsImages",
          public_id: typ
        },
        (error, result) => {
          if (error) {
            console.error(error);
            reject(error);
          } else {
            console.log(result);
            resolve(result);
          }
        }
      );
    });
  }
  
  const baseImg = async (imag,typ) => {
    const b64 = Buffer.from(imag.buffer).toString("base64");
    const dataURI = "data:" + imag.mimetype + ";base64," + b64;
    try {
      const resp = await handleUpload(dataURI, imag,typ);
      return resp;
    } catch (error) {
      throw error;
    }
  };
  
  module.exports = baseImg;