const redis=require('redis');

const tokenDb = redis.createClient({
    url:'redis://red-cll0ikuaov6s73f0vkn0:6379',
    db: 2
});

tokenDb.on('connect', () => {
    console.log('Connected to Redis server');
});

tokenDb.on('error', (err) => {
    console.error('Redis error:', err);
});

module.exports = tokenDb;
