const express = require('express');
const app = express();
const cors = require("cors");
const pass = require('./password');
const sessions = require('./session');
const baseImg = require('./img');

// Create a transporter object using SMTP transport

const {db,writeToFirestore} = require('./database')
const usersRef = db.collection('Users')
const EventDataRef = db.collection('EventData')

// const docRef = EventDataRef.doc('8xWqaNuFUdcaENQSpHmre');
const documentId = '8xWqaNuFUdcaENQSpHmre';


const { fetchDocumentById, fetchAllDocuments } =require('./getData');


const multer = require('multer');

const storage = new multer.memoryStorage();
const upload = multer({
  storage,
});

app.use(express.json({ limit: '5mb' }));
app.use(express.urlencoded({ extended: true, limit: '5mb' }));


const allowedOrigins = ['https://eventsdey.vercel.app', 'http://localhost:3000'];

app.use(
    cors({
        origin: allowedOrigins,
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        credentials: true,
    })
);

app.post('/create', (req, res) => {

    const { firstName, lastname, email, password } = req.body
    console.log(password)

    const id = email
    const result = pass.getPassword(password)

    const userObj = {
        email: email,
        firstName: firstName,
        lastName: lastname,
        salt: result.salt,
        hash: result.hash
    }

    writeToFirestore(usersRef.doc(id),userObj,res)
})

app.get('/sendMail',(req,res)=>{
  const mailOptions = {
    from: 'ssuunafran@gmail.com', // Sender email address
    to: 'ssuunafran@gmail.com', // Recipient email address
    subject: 'Test Email', // Email subject  alineffeedna@gmail.com
    text: 'This is a test email.' // Email body (plain text)
  };
  
  // Send email
  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.error(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
})

app.post('/retrieve', async (req, res) => {
    const { email, password } = req.body

    const UserEmail = await usersRef.where('email', '==', email).get();

    if (UserEmail.size > 0) {
        UserEmail.forEach((doc) => {
            const salt = doc.data().salt
            const hash = doc.data().hash
            const user = {
                id: doc.data().email,
                fname: doc.data().firstName,
                lname: doc.data().lastName
            }
            if ((pass.validatePassword(password, salt).hash) === hash) {
                sessions.generateAndSetTokens(user, 300, res)
            } else {
                return res.status(401).json({ message: 'Incorrect password' });
            }
        })
    } else {
        return res.status(401).json({ message: 'No User Found !' })
    }

})

const cpUpload = upload.fields([{ name: 'banner', maxCount: 1 }, { name: 'thumbnail', maxCount: 1 }])

app.post('/publish', cpUpload, async (req, res) => {

    try {
        const resp1 = await baseImg(req.files.banner[0],req.body.eventId+'B');
        const resp2 = await baseImg(req.files.thumbnail[0],req.body.eventId+'T');

        req.body.bannerFormat = resp1.format 
        req.body.thumbnailFormat = resp1.format 
      
        writeToFirestore(EventDataRef.doc(req.body.eventId), req.body, res);
      } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: "Internal Server Error" });
      }
})

app.get('/getEvents',(req,res)=>{
    fetchAllDocuments(EventDataRef)
  .then((documents) => {
    if (documents.length > 0) {
      console.log('All documents:', documents);
      res.status(200).json(documents)
    } else {
      console.log('The collection is empty.');
      res.status(401).json({ message: "No Events Found" });
    }
  })
  .catch((error) => {
    console.error('Error:', error);
  });
})

app.post('/getEvent',(req,res)=>{
  const documentId=req.body.eventId
  fetchDocumentById(EventDataRef, documentId)
  .then((documentData) => {
    if (documentData) {
      console.log('Document data:', documentData);
      res.status(200).json(documentData); // Assuming you're in an Express context
    } else {
      console.log('Document not found for ID:', documentId);
      res.status(401).json({ message: "Document Not Found" }); // Use a 404 status
    }
  })
  .catch((error) => {
    console.error('Error fetching document:', error);
    res.status(500).json({ message: "Error Fetching Data" });
  });
})

const port = process.env.PORT || 9080;
app.listen(port, (req, res) => {
    console.log(`Listening at ${port}`)
})
