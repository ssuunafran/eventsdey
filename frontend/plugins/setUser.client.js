export default defineNuxtPlugin(nuxtApp => {
    
    const user = useCookie('userCookie')

    if (user.value != null){
        initUser(user.value)
    }
})