import { useUserData } from '../pages/profile/decodedData';
import db from '../../backend/redis';
import { useCounterStore } from '../assets/store/store'

export default defineNuxtRouteMiddleware(async (to, from) => {
    const store = useCounterStore()
   
    if (!store.getToken) {
        console.log('no user')
        return navigateTo('/accounts/login')
    }
})     