interface FetchOptions {
  method?: string;
  headers?: { [key: string]: string };
  body?: string | FormData;
}

export const User = async (
  url: string,
  options: FetchOptions
): Promise<any> => {
  try {
    const response = await fetch(url, options);
    if (!response.ok) {
      const jsonData = await response.json();
      return {
        data: jsonData,
        status: response.status,
      };
    }
    const jsonData = await response.json();

    return {
      data: jsonData,
      status: response.status,
    };
        
  } catch (error) {
    console.error('Fetch error:', error);
    throw error;
  }
};
